#include <stdio.h>
#include <SDL.h>
#include "game.h"

int main(int argc, char *argv[])
{
	SDL_Window *windowPtr = NULL;

	// Init SDL2
	auto init = SDL_Init(SDL_INIT_EVERYTHING);

	// Check for errors
	if (init != 0)
	{
		printf("SDL init error: %s\n", SDL_GetError());
		return 1;
	}

	//Create window
	windowPtr = SDL_CreateWindow("Hello World!", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 480, 320, SDL_WINDOW_RESIZABLE);

	// Check for errors
	if (windowPtr == NULL)
	{
		printf("SDL create window error: %s\n", SDL_GetError());
		return 1;
	}
	Game_Initialize();
	Game_Run();
	Game_Stop();
	Game_Shutdown();
	
	SDL_DestroyWindow(windowPtr);
	windowPtr = NULL;
	SDL_Quit();

	return 0;
}