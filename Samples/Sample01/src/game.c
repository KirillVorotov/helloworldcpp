#include "game.h"
#include <SDL.h>
#include <stdbool.h>
#include <stdio.h>
#include "point2.h"

bool running = false;
SDL_Event event;
typedef struct Time
{
	double realtimeSinceStartup;
	double maxFrameTime;
	double deltaTime;
	double timeAccumulator;
	double fixedTime;
	double fixedDeltaTime;
	double fixedTimeAccumulator;
} Time;

Time Game_time;
Point2 mousePosition;
Point2 prevMousePosition;
Point2 mouseDelta;

void Game_Initialize(void)
{
	Game_time.realtimeSinceStartup = 0.0;
	Game_time.maxFrameTime = 0.250;
	Game_time.deltaTime = 0.002;
	Game_time.timeAccumulator = 0.0;
	Game_time.fixedTime = 0.0;
	Game_time.fixedDeltaTime = 0.020;
	Game_time.fixedTimeAccumulator = 0.0;
}

void Game_Run(void)
{
	running = true;

	Game_time.realtimeSinceStartup = (double)(SDL_GetTicks()) / 1000.0;
	Game_time.fixedTime = Game_time.realtimeSinceStartup;

	/*Main loop*/
	while (running)
	{
		double newTime = (double)SDL_GetTicks() / 1000.0;
		double frameTime = newTime - Game_time.realtimeSinceStartup;
		if (frameTime > Game_time.maxFrameTime)
			frameTime = Game_time.maxFrameTime;
		Game_time.realtimeSinceStartup = newTime;

		/*Fixed Update*/
		Game_time.fixedTimeAccumulator += frameTime;
		while (Game_time.fixedTimeAccumulator >= Game_time.fixedDeltaTime)
		{
			Game_time.fixedTime += Game_time.fixedDeltaTime;
			Game_FixedUpdate(Game_time.fixedTime, Game_time.fixedDeltaTime);
			Game_time.fixedTimeAccumulator -= Game_time.fixedDeltaTime;
		}

		/*Update, render*/
		Game_time.timeAccumulator += frameTime;
		if (Game_time.timeAccumulator >= Game_time.deltaTime)
		{
			Game_Update(Game_time.realtimeSinceStartup, Game_time.timeAccumulator);
			Game_time.timeAccumulator = 0.0;
		}
	}
}

void Game_FixedUpdate(double t, double dt)
{

}

void Game_Update(double t, double dt)
{
	SDL_GetGlobalMouseState(&mousePosition.x, &mousePosition.y);
	mouseDelta = point2_sub(mousePosition, prevMousePosition);
	prevMousePosition = mousePosition;

	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT)
		{
			Game_Stop();
		}
	}
}

void Game_Stop(void)
{
	running = false;
}

void Game_Shutdown(void)
{

}