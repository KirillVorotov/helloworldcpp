#ifndef game_h
#define game_h

void Game_Initialize(void);
void Game_Run(void);
void Game_FixedUpdate(double t, double dt);
void Game_Update(double t, double dt);
void Game_Stop(void);
void Game_Shutdown(void);

#endif // !game_h