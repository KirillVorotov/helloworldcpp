#ifndef vector2_h
#define vector2_h

typedef struct Vector2f
{
	float x, y;
} Vector2f;

typedef struct Vector2d
{
	double x, y;
} Vector2d;

/*Vector2f*/

Vector2f vector2f_new(float x, float y)
{
	return (Vector2f) { .x = x, .y = y };
}
Vector2f *vector2f_malloc(float x, float y)
{
	Vector2f *v = malloc(sizeof(Vector2f));
	v->x = x;
	v->y = y;
	return v;
}

static const struct Vector2f VECTOR2F_ZERO = { 0.0f, 0.0f };
static const struct Vector2f VECTOR2F_ONE = { 1.0f, 1.0f };
static const struct Vector2f VECTOR2F_UNITX = { 1.0f, 0.0f };
static const struct Vector2f VECTOR2F_UNITY = { 0.0f, 1.0f };

Vector2f vector2f_add(Vector2f a, Vector2f b)
{
	Vector2f result;
	result.x = a.x + b.x;
	result.y = a.y + b.y;
	return result;
}
Vector2f vector2f_sub(Vector2f a, Vector2f b)
{
	Vector2f result;
	result.x = a.x - b.x;
	result.y = a.y - b.y;
	return result;
}
Vector2f vector2f_mul(Vector2f a, Vector2f b)
{
	Vector2f result;
	result.x = a.x * b.x;
	result.y = a.y * b.y;
	return result;
}
Vector2f vector2f_div(Vector2f a, Vector2f b)
{
	Vector2f result;
	result.x = a.x / b.x;
	result.y = a.y / b.y;
	return result;
}

/*Vector2d*/

Vector2d vector2d_new(double x, double y)
{
	Vector2d result;
	result.x = x;
	result.y = y;
	return result;
}
Vector2d *vector2d_malloc(double x, double y)
{
	Vector2d *v = malloc(sizeof(Vector2d));
	v->x = x;
	v->y = y;
	return v;
}

static const struct Vector2d VECTOR2D_ZERO = { 0.0, 0.0 };
static const struct Vector2d VECTOR2D_ONE = { 1.0, 1.0 };
static const struct Vector2d VECTOR2D_UNITX = { 1.0, 0.0 };
static const struct Vector2d VECTOR2D_UNITY = { 0.0, 1.0 };

Vector2d vector2d_add(Vector2d a, Vector2d b)
{
	Vector2d result;
	result.x = a.x + b.x;
	result.y = a.y + b.y;
	return result;
}
Vector2d vector2d_sub(Vector2d a, Vector2d b)
{
	Vector2d result;
	result.x = a.x - b.x;
	result.y = a.y - b.y;
	return result;
}
Vector2d vector2d_mul(Vector2d a, Vector2d b)
{
	Vector2d result;
	result.x = a.x * b.x;
	result.y = a.y * b.y;
	return result;
}
Vector2d vector2d_div(Vector2d a, Vector2d b)
{
	Vector2d result;
	result.x = a.x / b.x;
	result.y = a.y / b.y;
	return result;
}

#endif // !vector2_h