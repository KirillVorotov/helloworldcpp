#ifndef point2_h
#define point2_h

typedef struct Point2
{
	int x, y;
} Point2;

Point2 point2_add(Point2 pointA, Point2 pointB);
Point2 point2_sub(Point2 pointA, Point2 pointB);
Point2 point2_mul(Point2 pointA, Point2 pointB);

Point2 point2_new(int x, int y)
{
	return(Point2) { .x = x, .y = y };
}

Point2 *point2_malloc(int x, int y)
{
	Point2 *point = malloc(sizeof(Point2));
	point->x = x;
	point->y = y;
	return point;
}

Point2 point2_add(Point2 pointA, Point2 pointB)
{
	Point2 result;
	result.x = pointA.x + pointB.x;
	result.y = pointA.y + pointB.y;
	return result;
}
Point2 point2_sub(Point2 pointA, Point2 pointB)
{
	Point2 result;
	result.x = pointA.x - pointB.x;
	result.y = pointA.y - pointB.y;
	return result;
}
Point2 point2_mul(Point2 pointA, Point2 pointB)
{
	Point2 result;
	result.x = pointA.x * pointB.x;
	result.y = pointA.y * pointB.y;
	return result;
}

#endif // !point2_h

