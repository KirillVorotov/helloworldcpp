#include <stdio.h>
#include <stdbool.h>
#include <SDL.h>

#include <d3d11.h>

// DirectX 12 specific headers.
#include <d3d12.h>
#include <dxgi1_6.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>

// D3D12 extension library.
#include <d3dx12.h>

bool running = 1;

char *base_path = NULL;

SDL_Window *windowPtr = NULL;
SDL_Event event;
SDL_Surface *image = NULL;
SDL_Surface *windowSurfacePtr = NULL;

unsigned int realtimeSinceStartup = 0;

unsigned int tFixed = 0;
unsigned int fixedDeltaTime = 20;
unsigned int accumulatorFixed = 0;

unsigned int deltaTime = 2;
unsigned int accumulator = 0;

int Initialize()
{
	base_path = SDL_GetBasePath();
	printf("Base path: %s\n", base_path);

	// Init SDL2
	auto init = SDL_Init(SDL_INIT_EVERYTHING);

	// Check for errors
	if (init != 0)
	{
		printf("SDL init error: %s\n", SDL_GetError());
		return 1;
	}

	//Create window
	windowPtr = SDL_CreateWindow("Hello World!", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 480, 320, SDL_WINDOW_RESIZABLE);

	// Check for errors
	if (windowPtr == NULL)
	{
		printf("SDL create window error: %s\n", SDL_GetError());
		return 1;
	}

	// Get window surface
	windowSurfacePtr = SDL_GetWindowSurface(windowPtr);

	// Check for errors
	if (windowSurfacePtr == NULL)
	{
		printf("SDL get window surface error: %s\n", SDL_GetError());
		return 1;
	}

	return 0;
}

void FixedUpdate(unsigned int t, unsigned int dt)
{

}

void Update(unsigned int t, unsigned int dt)
{
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
			running = 0;
			break;
		default:
			break;
		}
	}
}

void UpdateSurface(SDL_Surface *surface)
{

}

void Render()
{

}

void Shutdown()
{
	SDL_FreeSurface(windowSurfacePtr);
	windowSurfacePtr = NULL;
	SDL_DestroyWindow(windowPtr);
	windowPtr = NULL;
	SDL_Quit();
	printf("Shutdown\n");
	printf("Time: %i ms\n", realtimeSinceStartup);
}

int main(int argc, char *argv[])
{
	int init = Initialize();
	if (init != 0)
		return init;

	// Set time variables
	realtimeSinceStartup = SDL_GetTicks();
	tFixed = realtimeSinceStartup;

	// Main loop
	while (running != 0)
	{
		unsigned int newTime = SDL_GetTicks();
		unsigned int frameTime = newTime - realtimeSinceStartup;
		if (frameTime > 250LL)
			frameTime = 250LL;
		realtimeSinceStartup = newTime;

		// Fixed Update
		accumulatorFixed += frameTime;
		while (accumulatorFixed >= fixedDeltaTime)
		{
			tFixed += fixedDeltaTime;
			FixedUpdate(tFixed, fixedDeltaTime);
			accumulatorFixed -= fixedDeltaTime;
		}

		// Update, render
		accumulator += frameTime;
		if (accumulator >= deltaTime)
		{
			Update(realtimeSinceStartup, accumulator);
			Render();
			accumulator = 0LL;
		}
	}

	Shutdown();
	return 0;
}