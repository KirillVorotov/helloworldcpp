#include <SDL.h>

class Game
{
public:
	Game();
	~Game();
	int Initialize();
	void Update(Uint32 t, Uint32 dt);
	void FixedUpdate(Uint32 t, Uint32 dt);
	int Shutdown();
private:
	bool quit = false;

	Uint32 realtimeSinceStartup = 0;

	Uint32 timeFixed = 0;
	Uint32 deltaTimeFixed = 20;
	Uint32 accumulatorFixed = 0;

	Uint32 deltaTime = 2;
	Uint32 accumulator = 0;
};