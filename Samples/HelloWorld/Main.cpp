#include <SDL.h>
#include <mono/jit/jit.h>
#include <mono/metadata/mono-config.h>
#include <mono/metadata/assembly.h>
#include <iostream>
#include <string>
#include <Game.h>

using namespace std;

bool quit = false;

Game *game = nullptr;

SDL_Window *windowPtr = nullptr;
SDL_Event *eventPtr = nullptr;
SDL_Surface *image = nullptr;
SDL_Surface *windowSurfacePtr = nullptr;

unsigned int realtimeSinceStartup = 0;

unsigned int tFixed = 0;
unsigned int fixedDeltaTime = 20;
unsigned int accumulatorFixed = 0;

unsigned int deltaTime = 2;
unsigned int accumulator = 0;

int InitializeMono()
{
	mono_set_dirs("D:/Git/KirillVorotov/HelloWorldCpp/Bin/x64/Debug/mono/lib", "D:/Git/KirillVorotov/HelloWorldCpp/Bin/x64/Debug/mono/etc");

	mono_config_parse(nullptr);

	MonoDomain* monoDomain = mono_jit_init("HelloWorld");

	if (monoDomain == nullptr)
	{
		cout << "mono_jit_init failed!" << endl;
		return 1;
	}

	// open our Example.dll assembly
	MonoAssembly* assembly = mono_domain_assembly_open(monoDomain, "D:/Git/KirillVorotov/HelloWorldCpp/Bin/x64/Debug/Example.dll");

	if (assembly == nullptr)
	{
		cout << "mono_domain_assembly_open failed!" << endl;
		return 1;
	}

	MonoImage* monoImage = mono_assembly_get_image(assembly);

	if (monoImage == nullptr)
	{
		cout << "mono_assembly_get_image failed!" << endl;
		return 1;
	}

	// find the Entity class in the image
	MonoClass* entityClass = mono_class_from_name(monoImage,
		"Example",
		"Entity");

	// allocate memory for one Entity instance
	MonoObject* entityObject = mono_object_new(monoDomain, entityClass);

	// find the constructor method that takes one parameter
	MonoMethod* constructorMethod = mono_class_get_method_from_name(entityClass,
		".ctor",
		1);

	// create a MonoString that will be passed to the constructor as an argument
	MonoString* name = mono_string_new(mono_domain_get(), "Kirill");
	void* args[1];
	args[0] = name;

	// finally, invoke the constructor
	MonoObject* exception = nullptr;
	mono_runtime_invoke(constructorMethod, entityObject, args, &exception);

	// find the Process method that takes zero parameters
	MonoMethod* processMethod = mono_class_get_method_from_name(entityClass,
		"Process",
		0);
	exception = nullptr;

	// invoke the method
	// if invoking static methods, then the second argument must be null
	mono_runtime_invoke(processMethod, entityObject, nullptr, &exception);

	// check for any thrown exception
	if (exception)
	{
		std::cout << mono_string_to_utf8(mono_object_to_string(exception, nullptr))
			<< std::endl;
	}

	// find the GetName method
	MonoMethod* getNameMethod = mono_class_get_method_from_name(entityClass,
		"GetName",
		0);
	exception = nullptr;
	MonoString* ret = (MonoString*)mono_runtime_invoke(getNameMethod, entityObject, nullptr, &exception);
	char* c = mono_string_to_utf8(ret);
	std::cout << "Value of 'Name' is " << c << std::endl;
	// free the memory allocated from mono_string_to_utf8 ()
	mono_free(c);

	// find the Id field in the Entity class
	MonoClassField* idField = mono_class_get_field_from_name(entityClass, "Id");
	int value = 42;

	// set the field's value
	mono_field_set_value(entityObject, idField, &value);

	int result;
	mono_field_get_value(entityObject, idField, &result);
	std::cout << "Value of 'Id' is " << result << std::endl;

	mono_jit_cleanup(monoDomain);

	return 0;
}

void Initialize()
{
	game = new Game();
}

void FixedUpdate(unsigned int t, unsigned int dt)
{

}

void Update(unsigned int t, unsigned int dt)
{
	while (SDL_PollEvent(eventPtr))
	{
		switch (eventPtr->type)
		{
		case SDL_QUIT:
			quit = true;
			break;
		default:
			break;
		}
	}
}

void UpdateSurface(SDL_Surface *surface)
{

}

void Render()
{

}

void Shutdown()
{
	delete game;
	game = nullptr;
	delete eventPtr;
	eventPtr = nullptr;
	SDL_FreeSurface(windowSurfacePtr);
	windowSurfacePtr = nullptr;
	SDL_DestroyWindow(windowPtr);
	windowPtr = nullptr;
	SDL_Quit();

	cout << "Shutdown" << endl;
}

int main(int argc, char *argv[])
{
	// Init
	auto init = SDL_Init(SDL_INIT_EVERYTHING);

	// Check for errors
	if (init != 0)
	{
		cout << "SDL init error: " << SDL_GetError() << endl;
		return 1;
	}

	//Create window
	windowPtr = SDL_CreateWindow("Hello World!", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 480, 320, SDL_WINDOW_RESIZABLE);

	// Check for errors
	if (windowPtr == nullptr)
	{
		cout << "SDL create window error: " << SDL_GetError() << endl;
		return 1;
	}

	// Create event
	eventPtr = new SDL_Event();

	// Get window surface
	windowSurfacePtr = SDL_GetWindowSurface(windowPtr);

	// Check for errors
	if (windowSurfacePtr == nullptr)
	{
		cout << "SDL get window surface error: " << SDL_GetError() << endl;
		return 1;
	}

	// Set time variables
	realtimeSinceStartup = SDL_GetTicks();
	tFixed = realtimeSinceStartup;

	Initialize();
	if (InitializeMono() != 0)
	{
		return 1;
	}

	// Main loop
	while (!quit)
	{
		unsigned int newTime = SDL_GetTicks();
		unsigned int frameTime = newTime - realtimeSinceStartup;
		if (frameTime > 250LL)
			frameTime = 250LL;
		realtimeSinceStartup = newTime;

		// Fixed Update
		accumulatorFixed += frameTime;
		while (accumulatorFixed >= fixedDeltaTime)
		{
			tFixed += fixedDeltaTime;
			FixedUpdate(tFixed, fixedDeltaTime);
			accumulatorFixed -= fixedDeltaTime;
		}

		// Update, render
		accumulator += frameTime;
		if (accumulator >= deltaTime)
		{
			Update(realtimeSinceStartup, accumulator);
			Render();
			accumulator = 0LL;
		}
	}

	Shutdown();
	return 0;
}